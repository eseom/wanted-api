import json


def test_company_view(api):
    rv = api.get('/companies')
    data = json.loads(rv.data)
    assert 20 == len(data['records'])

    rv = api.get('/companies?q=wa&take=20')
    data = json.loads(rv.data)
    assert 2 == len(data['records'])
    assert [41, 2] == [c['id'] for c in data['records']]


def test_company_tag_view(api):
    rv = api.get('/tags/companies')
    data = json.loads(rv.data)
    assert 0 == len(data['records'])

    rv = api.get('/tags/companies?q=tag_11&take=20')
    data = json.loads(rv.data)
    assert 15 == len(data['records'])
    assert 15 == data['count']
    assert [35, 78] == [c['id'] for c in data['records']][:2]

    rv = api.get('/tags/companies?q=tag_11&take=20&skip=2')
    data = json.loads(rv.data)
    assert 13 == len(data['records'])
    assert 15 == data['count']
    assert [9, 56] == [c['id'] for c in data['records']][:2]


def test_company_tag_view_status_code(api):
    rv = api.post('/companies/1/tags', headers={
        'content-type': 'application/json'
    }, data=json.dumps(dict(
        name='new_tag_1',
        language='en',
    )))
    assert rv.status_code == 200

    rv = api.post('/companies/1/tags', headers={
        'content-type': 'application/json'
    }, data=json.dumps(dict(
        name='new_tag_1',
        language='en',
    )))
    assert rv.status_code == 409
    assert json.loads(rv.data)['message'] == 'tag already exist'

    rv = api.post('/companies/100011/tags', headers={
        'content-type': 'application/json'
    }, data=json.dumps(dict(
        name='new_tag_1',
        language='en',
    )))
    assert rv.status_code == 404
    assert json.loads(rv.data)['message'] == 'company not found'

    rv = api.delete('/companies/1/tags/775', headers={
        'content-type': 'application/json'
    })
    assert rv.status_code == 200

    rv = api.delete('/companies/10000/tags/775', headers={
        'content-type': 'application/json'
    })
    assert rv.status_code == 404
    assert json.loads(rv.data)['message'] == 'company not found'

    rv = api.delete('/companies/1/tags/333222', headers={
        'content-type': 'application/json'
    })
    assert rv.status_code == 404
    assert json.loads(rv.data)['message'] == 'tag not found'

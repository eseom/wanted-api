import pytest
from app import app, db


@pytest.fixture
def api():
    app.config['TESTING'] = True
    db.create_all()

    # test records
    from app.sample import seed_sample
    seed_sample()

    api = app.test_client()
    yield api

    db.session.rollback()
    db.drop_all()

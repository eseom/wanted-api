import pytest
from app.service import (CompanyNotFound, CompanyService, TagAlreadyExist,
                         TagNotFound, TagService)

company_service = CompanyService()
tag_service = TagService()


def _add_tag(company_id, tag_name, language):
    tag_service.add_tag(company_id, tag_name, language)


def test_company_service(api):
    companies = company_service.get_by_name('w', 2, 0)
    assert companies['count'] == 3
    assert len(companies['records']) == 2
    assert companies['records'][0].id == 41
    assert companies['records'][1].id == 2

    companies = company_service.get_by_tag('tag_11', 2, 0)
    assert companies['count'] == 15
    assert len(companies['records']) == 2
    assert companies['records'][0].id == 35
    assert companies['records'][1].id == 78


def test_tag_service(api):
    tag_name = 'tag_wantedlab'
    _add_tag(1, tag_name, 'ko')
    company = company_service.get(1)
    assert tag_name in [tag.name for tag in company.tags]
    tags = company_service.get(1).tags
    last_tag = tags[len(tags) - 1]
    last_tag.name = tag_name
    last_tag.language = 'ko'
    tag_service.remove_tag(1, last_tag.id)


def test_tag_service_with_exception(api):
    with pytest.raises(CompanyNotFound) as e:
        tag_service.add_tag(3035, 'tag_with_no_company', 'ko')
    assert 'company not found' in str(e.value)

    with pytest.raises(CompanyNotFound) as e:
        tag_service.remove_tag(3035, 11)
    assert 'company not found' in str(e.value)

    with pytest.raises(TagNotFound) as e:
        tag_service.remove_tag(1, 34344)
    assert 'tag not found' in str(e.value)

    tag_name = 'tag_wantedlab'
    _add_tag(1, tag_name, 'ko')

    company = company_service.get(1)
    assert len(company.tags) == 4

    with pytest.raises(TagAlreadyExist) as e:
        tag_name = 'tag_wantedlab'
        _add_tag(1, tag_name, 'ko')
    assert 'tag already exist' in str(e.value)

    tag_service.remove_tag(1, 775)

    company = company_service.get(1)
    assert len(company.tags) == 3

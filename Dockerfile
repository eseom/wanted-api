FROM python:3.7-alpine
RUN \
 apk add --no-cache postgresql-libs && \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev
COPY requirements.txt /
COPY requirements-test.txt /
RUN pip install -r /requirements.txt
RUN pip install -r /requirements-test.txt
RUN apk --purge del .build-deps
WORKDIR /
# for cli
ENV FLASK_APP app/__init__.py
ENV FLASK_ENV development
ENV FLASK_DEBUG 1
ENV PYTHONUNBUFFERED 0

import csv
import pprint
from os.path import dirname

from ..model import Company, CompanyName, Tag, db


def seed_sample():
    companies = []
    with open(f'{dirname(__file__)}/wanted_temp_data.csv', 'r') as fp:
        rdr = csv.reader(fp)
        for line in rdr:
            company_names = []
            tags = []
            if line[0]:
                company_names.append(CompanyName(
                    name=line[0],
                    language='ko',
                ))
            if line[1]:
                company_names.append(CompanyName(
                    name=line[1],
                    language='en',
                ))
            if line[2]:
                company_names.append(CompanyName(
                    name=line[2],
                    language='ja',
                ))
            if line[3]:
                for a in line[3].split('|'):
                    tags.append(Tag(
                        name=a.strip(),
                        language='ko',
                    ))
            if line[4]:
                for a in line[4].split('|'):
                    tags.append(Tag(
                        name=a.strip(),
                        language='en',
                    ))
            if line[5]:
                for a in line[5].split('|'):
                    tags.append(Tag(
                        name=a.strip(),
                        language='ja',
                    ))
            companies.append(Company(
                company_names=company_names,
                tags=tags,
            ))

    db.session.add_all(companies)
    db.session.commit()

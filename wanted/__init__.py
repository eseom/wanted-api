import os
from functools import wraps

from flasgger import Swagger
from flask import Flask, jsonify
from flask_migrate import Migrate
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get(
    'SQLALCHEMY_DATABASE_URI')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = not(
    not int(os.environ.get('SQLALCHEMY_ECHO', False)))

db = SQLAlchemy(app)
migrate = Migrate(app, db)
api = Api(app)
swagger = Swagger(app)


def get_http_exception_handler(app):
    handle_http_exception = app.handle_http_exception

    @wraps(handle_http_exception)
    def ret_val(exception):
        exc = handle_http_exception(exception)
        return jsonify(
            {'code': exc.code, 'message': str(exc.description)}), exc.code
    return ret_val


app.handle_http_exception = get_http_exception_handler(app)


@app.cli.command("sample")
def sample():
    from .sample import seed_sample
    seed_sample()


with app.app_context():
    from . import view  # noqa

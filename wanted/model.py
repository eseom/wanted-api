from datetime import datetime

from sqlalchemy import (Column, DateTime, ForeignKey, Integer,
                        PrimaryKeyConstraint, Table, Unicode, event)
from sqlalchemy.orm import Session, backref, relationship

from . import db

company_tag = Table(
    'company_tags', db.Model.metadata,
    Column('company_id', Integer, ForeignKey('companies.id')),
    Column('tag_id', Integer, ForeignKey('tags.id')),
    PrimaryKeyConstraint('company_id', 'tag_id')
)


class BaseMixin:
    id = Column(Integer, primary_key=True)
    created_at = Column('created_at', DateTime, nullable=False,
                        default=datetime.utcnow)
    updated_at = Column('updated_at', DateTime, nullable=False,
                        default=datetime.utcnow, onupdate=datetime.utcnow)


class Company(db.Model, BaseMixin):
    __tablename__ = 'companies'

    company_names = relationship('CompanyName',
                                 backref=backref(
                                     'company', uselist=False),
                                 uselist=True)
    tags = relationship(
        'Tag', secondary=company_tag,
        backref=backref('companies', uselist=True))


class CompanyName(db.Model, BaseMixin):
    __tablename__ = 'company_names'

    company_id = Column(Integer, ForeignKey('companies.id'))
    name = Column(Unicode, index=True)
    language = Column(Unicode)


@event.listens_for(Session, 'after_flush')
def receive_after_flush(session, ctx):
    session.begin(subtransactions=True)
    session.query(Tag).filter(
        ~Tag.companies.any()).delete(
        synchronize_session=False)
    session.commit()


class Tag(db.Model, BaseMixin):
    __tablename__ = 'tags'

    name = Column(Unicode, index=True)
    language = Column(Unicode)

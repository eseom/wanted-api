from sqlalchemy import and_
from sqlalchemy.orm import joinedload

from . import db
from .model import Company, CompanyName, Tag


class CompanyNotFound(Exception):
    def __str__(self):
        return 'company not found'


class TagAlreadyExist(Exception):
    def __str__(self):
        return 'tag already exist'


class TagNotFound(Exception):
    def __str__(self):
        return 'tag not found'


class TagService:
    def add_tag(self, company_id, name, language):
        company = Company.query.options(joinedload('tags')).get(company_id)
        if not company:
            raise CompanyNotFound()
        if name in [t.name for t in company.tags]:
            raise TagAlreadyExist()
        tag = Tag.query.filter(Tag.name == name).first()
        if not tag:
            tag = Tag(
                name=name,
                language=language,
            )
        company.tags.append(tag)
        db.session.add(company)
        db.session.commit()

    def remove_tag(self, company_id, tag_id):
        company = Company.query.options(joinedload('tags')).get(company_id)
        if not company:
            raise CompanyNotFound()
        tag = Tag.query.get(tag_id)
        if not tag:
            raise TagNotFound()
        company.tags = [t for t in company.tags if t.id != tag_id]
        db.session.add(company)
        db.session.commit()


class CompanyService:
    def get_by_name(self, q, take, skip):
        query = Company.query.join(CompanyName, and_(
            CompanyName.company_id == Company.id,
            CompanyName.name.ilike(f"%{q}%"))
        ).group_by(Company.id, CompanyName.name)
        return dict(
            count=query.group_by(Company.id).count(),
            records=query.order_by(
                CompanyName.name.asc()).options(
                    joinedload('company_names')).offset(skip).limit(take).all()
        )

    def get_by_tag(self, q, take, skip):
        query = Company.query.filter(
            Company.tags.any(name=q)
        ).join(
            CompanyName, Company.company_names
        ).options(
            joinedload('company_names')
        )
        return dict(
            count=query.group_by(Company.id).count(),
            records=query.order_by(
                CompanyName.name.asc()
            ).offset(skip).limit(take).all(),
        )

    def get(self, id):
        return Company.query.get(id)

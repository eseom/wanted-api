from flask import abort, request
from flask_restful import Resource
from marshmallow import exceptions

from . import api
from .schema import (AddTagRequest, AddTagResponse, DeleteTagResponse,
                     ListCompanyByTagRequest, ListCompanyByTagResponse,
                     ListCompanyRequest, ListCompanyResponse)
from .service import (CompanyNotFound, CompanyService, TagAlreadyExist,
                      TagNotFound, TagService)


def marshal(request_klass, response_klass):
    def load(a):
        if not request_klass:
            return a
        try:
            return request_klass().load(a)
        except exceptions.ValidationError as e:
            abort(400, e)

    def wrapper(func):
        def decorator(*args, **kwargs):
            request.load = load
            return response_klass().dump(func(*args, **kwargs))
        decorator.__doc__ = func.__doc__
        return decorator
    return wrapper


class CompanyView(Resource):
    @marshal(ListCompanyRequest, ListCompanyResponse)
    def get(self):
        """
        회사명 검색
        ---
        tags:
          - company
        parameters:
          - in: query
            name: q
            required: false
            description: 겁색어
            type: string
          - in: query
            name: take
            required: false
            description: take
            type: number
          - in: query
            name: skip
            required: false
            description: skip
            type: string
        responses:
          200:
            description: company list
            schema:
              id: CompanyListResponse
              type: object
              properties:
                count:
                  type: integer
                records:
                  type: array
                  items:
                    properties:
                      id:
                        type: integer
                      company_names:
                        type: array
                        items:
                          properties:
                            name:
                              type: string
                            language:
                              type: string
        """
        return CompanyService().get_by_name(**request.load(request.args))


class CompanyTagView(Resource):
    @marshal(ListCompanyByTagRequest, ListCompanyByTagResponse)
    def get(self):
        """
        태그로 회사 검색
        ---
        tags:
          - company
          - tag
        parameters:
          - in: query
            name: q
            required: false
            description: 겁색어
            type: string
          - in: query
            name: take
            required: false
            description: take
            type: number
          - in: query
            name: skip
            required: false
            description: skip
            type: string
        responses:
          200:
            description: company list
            schema:
              id: CompanyListResponse
              type: object
              properties:
                count:
                  type: integer
                records:
                  type: array
                  items:
                    properties:
                      id:
                        type: integer
                      company_names:
                        type: array
                        items:
                          properties:
                            name:
                              type: string
                            language:
                              type: string
        """
        return CompanyService().get_by_tag(**request.load(request.args))


class TagListView(Resource):
    @marshal(AddTagRequest, AddTagResponse)
    def post(self, company_id):
        """
        회사에 태그 추가
        ---
        tags:
          - tag
        parameters:
          - in: path
            name: company_id
            type: number
            required: true
          - name: body
            in: body
            required: true
            schema:
              id: Tag
              required:
                - name
                - language
              properties:
                name:
                  type: string
                  description: tag name
                language:
                  type: string
                  description: tag language
        responses:
          200:
            description: success
          404:
            description: company not found
            schema:
              id: CompanyNotFound
              type: object
              properties:
                code:
                  type: integer
                  example: 404
                message:
                  type: string
                  example: company not found
          409:
            description: tag already exist
            schema:
              id: TagNotFound
              type: object
              properties:
                code:
                  type: integer
                  example: 409
                message:
                  type: string
                  example: tag already exist
        """
        try:
            TagService().add_tag(company_id, **request.load(request.json))
        except CompanyNotFound as e:
            abort(404, e)
        except TagAlreadyExist as e:
            abort(409, e)


class TagView(Resource):
    @marshal(None, DeleteTagResponse)
    def delete(self, company_id, tag_id):
        """
        회사에서 태그 삭제
        ---
        tags:
          - tag
        parameters:
          - in: path
            name: company_id
            type: number
            required: true
          - in: path
            name: tag_id
            type: number
            required: true
        responses:
          200:
            description: success
          404:
            description: company not found | tag not found
            schema:
              id: TagAlreadyExist
              type: object
              properties:
                code:
                  type: integer
                  example: 404
                message:
                  type: string
                  example: company not found | tag not found
        """
        try:
            TagService().remove_tag(company_id, tag_id)
        except CompanyNotFound as e:
            abort(404, e)
        except TagNotFound as e:
            abort(404, e)


api.add_resource(CompanyView, '/companies')
api.add_resource(CompanyTagView, '/tags/companies')
api.add_resource(TagListView, '/companies/<int:company_id>/tags')
api.add_resource(TagView, '/companies/<int:company_id>/tags/<int:tag_id>')

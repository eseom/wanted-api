import os
from datetime import datetime

from flasgger import Swagger
from flask import Flask, abort, request
from flask_migrate import Migrate
from flask_restful import Api, Resource
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, exceptions, fields
from sqlalchemy import (Column, DateTime, ForeignKey, Integer, Table, Unicode,
                        and_, event)
from sqlalchemy.orm import Session, backref, joinedload, relationship


class BaseListRequest(Schema):
    q = fields.Str(missing='')
    take = fields.Integer(missing=20)
    skip = fields.Integer(missing=0)


class ListCompanyRequest(BaseListRequest):
    pass


class CompanyNameSchema(Schema):
    language = fields.Str()
    name = fields.Str()
    company_id = fields.Integer()


class TagSchema(Schema):
    language = fields.Str()
    name = fields.Str()
    company_id = fields.Integer()


class CompanySchema(Schema):
    id = fields.Integer()
    name = fields.Str()
    company_names = fields.List(fields.Nested(
        CompanyNameSchema, exclude=['company_id']))
    # tags = fields.List(fields.Nested(TagSchema, exclude=['company_id']))


class ListCompanyResponse(Schema):
    count = fields.Integer()
    records = fields.List(fields.Nested(CompanySchema))


class ListCompanyByTagRequest(BaseListRequest):
    pass


class ListCompanyByTagResponse(Schema):
    count = fields.Integer()
    records = fields.List(fields.Nested(CompanySchema))


class AddTagRequest(Schema):
    name = fields.Str(required=True)
    language = fields.Str(required=True)


class AddTagResponse(Schema):
    result = fields.Boolean()


class DeleteTagResponse(Schema):
    result = fields.Boolean()

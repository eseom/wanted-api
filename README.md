# Wanted code

# features
- 간단한 디렉토리 구조와 함께 docker-compose 환경으로 구성했습니다.
- pip 관리툴은 이용하지 않았습니다.
- 개발환경이므로 `wanted` directory를 volume으로 두고 `gunicorn --reload`가 설정되어 있습니다.
- swagger: flasgger, marshmallow 사용
- database: flask-sqlalchemy, flask-migrate 사용
- test: pytest, pytest-cov, codecov 사용

# docker containers

- wanted-api
- wanted-api-db
- wanted-test (pytest-watch container)
- wanted-test-db

# run
```
cd $(PROJECT_ROOT)
docker-compose build
docker-compose up -d
docker-compose exec wanted-api /bin/sh

(in container shell)
  # flask db upgrade
  # flask sample
# open http://localhost:8000/apidocs/
```

# log
```
docker-compose logs -f wanted-api
```

# pytest result
```
docker-compose logs -f wanted-test

(sample)
wanted-test_1     | ----------- coverage: platform linux, python 3.7.7-final-0 -----------
wanted-test_1     | Name                      Stmts   Miss  Cover   Missing
wanted-test_1     | -------------------------------------------------------
wanted-test_1     | /app/__init__.py             27      2    93%   39-40
wanted-test_1     | /app/model.py                26      0   100%
wanted-test_1     | /app/sample/__init__.py      29      0   100%
wanted-test_1     | /app/schema.py               43      0   100%
wanted-test_1     | /app/service.py              45      0   100%
wanted-test_1     | /app/view.py                 47      3    94%   16, 19-20
wanted-test_1     | -------------------------------------------------------
wanted-test_1     | TOTAL                       217      5    98%

```

# database
```
docker-compose exec wanted-api-db psql --user wanted
```
